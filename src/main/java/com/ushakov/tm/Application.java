package com.ushakov.tm;

import com.ushakov.tm.api.ICommandRepository;
import com.ushakov.tm.constant.ArgumentConst;
import com.ushakov.tm.constant.TerminalConst;
import com.ushakov.tm.model.Command;
import com.ushakov.tm.repository.CommandRepository;
import com.ushakov.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    private final static ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    private final static Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while(true) {
            System.out.println("ENTER COMMAND");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT: showAbout(); break;
            case ArgumentConst.ARG_VERSION: showVersion(); break;
            case ArgumentConst.ARG_HELP: showHelp(); break;
            case ArgumentConst.ARG_INFO: showSystemInfo(); break;
            default: showIncorrectArgument();
        }
    }

    private static void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT: showAbout(); break;
            case TerminalConst.CMD_VERSION: showVersion(); break;
            case TerminalConst.CMD_HELP: showHelp(); break;
            case TerminalConst.CMD_INFO: showSystemInfo(); break;
            case TerminalConst.CMD_COMMANDS: showCommands(); break;
            case TerminalConst.CMD_ARGUMENTS: showArguments(); break;
            case TerminalConst.CMD_EXIT: exit(); break;
            default: showIncorrectMessage();
        }
    }

    private static void showIncorrectMessage() {
        System.out.println("Error! Command is not correct...");
    }

    private static void showIncorrectArgument() {
        System.out.println("Error! Argument is not correct...");
    }

    private static void exit() {
        System.exit(0);
    }

    private static boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ivan Ushakov");
        System.out.println("E-MAIL: iushakov@tsconsulting.com");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showCommands() {
        for (final Command command: commands) System.out.println(command.getName());
    }

    private static void showArguments() {
        for (final Command command: commands)
            if (command.getArg() != null) System.out.println(command.getArg());
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        for (final Command command: commands) {
            final String name = command.getName();
            if (name == null) continue;
            System.out.println(command);
        }
    }

    private static void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " +  NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
